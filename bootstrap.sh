#!/usr/bin/env bash

## Shell options
set -e -u -x

# Enable CodeReady Linux Builder repository: 
# https://www.ltsplus.com/linux/oracle-linux-enable-epel-powertools
echo -e "\nEnabling CodeReady Linux Builder (PowerTools)...\n"
sudo dnf config-manager --set-enabled ol8_codeready_builder

# Install EPEL repository: 
# https://www.ltsplus.com/linux/oracle-linux-enable-epel-powertools
echo -e "\nEnabling EPEL repository...\n"
sudo dnf install -y oracle-epel-release-el8
# Enable EPEL repository
sudo dnf config-manager --set-enabled ol8_developer_EPEL

# Install Ansible
echo -e "\nInstalling Ansible...\n"
sudo dnf install -y ansible-collection-community-general

# Install Git
echo -e "\nInstalling Git...\n"
sudo dnf install -y git

# Clone Ansible playbook
echo -e "\nCloning and setting up Ansible provisioning playbook...\n"
git clone https://gitlab.com/penyuan/ansible-ol.git
# Pull submodules for Ansible roles
cd ansible-ol
git submodule init && git submodule update

# Run Ansible to provision this system
echo -e "\nRunning Ansible...\n"
ansible-playbook setup.yml -v -i HOSTS --become